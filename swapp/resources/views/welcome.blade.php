<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>window.Laravel = {csrfToken: '{{csrf_token()}}'} </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
    <title>Swap</title>
    <link rel="shortcut icon" type="image/x-icon"
          href="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRwWGDoYoS4Nk3xmXYDHEi4e_AybLyDhEtQ247BnNrQb6oVzRsJ"/>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">


</head>
<body>
<div id="app">

    <section class="hero is-large is-bold" style="
                                                        background-image: url(../images/table_office_flowers_113857_1920x1080.jpg);
                                                        background-size: cover;
                                                        background-position: center center;">
        <div class="hero-head">
            <nav class="navbar">
                <div class="container">
                    <div class="navbar-brand">
                        <figure class="navbar-item image is-128x128">
                            <img
                                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRwWGDoYoS4Nk3xmXYDHEi4e_AybLyDhEtQ247BnNrQb6oVzRsJ"
                                alt="Logo">
                        </figure>
                        <span class="navbar-burger burger" data-target="navbarMenuHeroB">
            <span></span>
            <span></span>
            <span></span>
          </span>
                    </div>
                    <div id="navbarMenuHeroB" class="navbar-menu">
                        <div class="navbar-end">
                            <a class="navbar-item">
                                Home
                            </a>
                            <a class="navbar-item">
                                Examples
                            </a>
                            <a class="navbar-item">
                                Documentation
                            </a>
                            <span class="navbar-item">
            </span>
                        </div>
                    </div>
                </div>
            </nav>
        </div>

        <div class="hero-body">
            <div class="container has-text-centered">
                <div class="columns">
                    <div class="column">
                        <div class="notification is-link">
                            <h3 class="title is-size-5">Seç ve Dağıt</h3>
                            <div class="control">
                                <label class="radio">
                                    <input type="radio" name="answer">
                                    Oda-1
                                </label>
                                <label class="radio">
                                    <input type="radio" name="answer">
                                    Oda-2
                                </label>
                                <label class="radio">
                                    <input type="radio" name="answer">
                                    Oda-3
                                </label>
                            </div>
                            <br>
                            <a class="button is-warning is-fullwidth"><strong>Dağıt</strong></a>

                        </div>
                    </div>
                    <div class="column">
                        <div class="notification is-primary">
                            <h3 class="title is-size-5">Çalışanlar ve Masaları</h3>
                            <div v-if="isLoggedIn">
                                <zafer></zafer>
                            </div>

                        </div>
                    </div>
                    <div class="column">
                        <div class="notification is-link">
                            <a class="button is-warning"><strong>Ortak Alan</strong></a>
                            <br>
                            <br>
                            <a class="button is-warning"><strong>İşlemleri Sıfırla</strong></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>


<script src="{{ asset('js/app.js')}}"></script>
</body>
</html>
